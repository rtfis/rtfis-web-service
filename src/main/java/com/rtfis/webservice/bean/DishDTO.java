package com.rtfis.webservice.bean;

import org.springframework.web.multipart.MultipartFile;

public class DishDTO {
    private MultipartFile dishPic;
    private String dishFileGroup;
    private String dishName;
    private String dishDescription;
    private String userName;

    public MultipartFile getDishPic() {
        return dishPic;
    }

    public void setDishPic(MultipartFile dishPic) {
        this.dishPic = dishPic;
    }

    public String getDishFileGroup() {
        return dishFileGroup;
    }

    public void setDishFileGroup(String dishFileGroup) {
        this.dishFileGroup = dishFileGroup;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public String getDishDescription() {
        return dishDescription;
    }

    public void setDishDescription(String dishDescription) {
        this.dishDescription = dishDescription;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
