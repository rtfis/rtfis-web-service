package com.rtfis.webservice.entities;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "dish", schema = "rtfis", catalog = "")
public class DishEntity {
    private int dishId;
    private String dishName;
    private int dishTypeId;
    private String dishDescription;
    private Integer imageId;
    private Date dishCreateDate;
    private String dishCreateBy;
    private Timestamp dishUpdateDate;
    private String dishUpdateBy;

    @Id
    @Column(name = "dish_id")
    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    @Basic
    @Column(name = "dish_name")
    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    @Basic
    @Column(name = "dish_type_id")
    public int getDishTypeId() {
        return dishTypeId;
    }

    public void setDishTypeId(int dishTypeId) {
        this.dishTypeId = dishTypeId;
    }

    @Basic
    @Column(name = "dish_description")
    public String getDishDescription() {
        return dishDescription;
    }

    public void setDishDescription(String dishDescription) {
        this.dishDescription = dishDescription;
    }

    @Basic
    @Column(name = "image_id")
    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    @Basic
    @Column(name = "dish_create_date")
    public Date getDishCreateDate() {
        return dishCreateDate;
    }

    public void setDishCreateDate(Date dishCreateDate) {
        this.dishCreateDate = dishCreateDate;
    }

    @Basic
    @Column(name = "dish_create_by")
    public String getDishCreateBy() {
        return dishCreateBy;
    }

    public void setDishCreateBy(String dishCreateBy) {
        this.dishCreateBy = dishCreateBy;
    }

    @Basic
    @Column(name = "dish_update_date")
    public Timestamp getDishUpdateDate() {
        return dishUpdateDate;
    }

    public void setDishUpdateDate(Timestamp dishUpdateDate) {
        this.dishUpdateDate = dishUpdateDate;
    }

    @Basic
    @Column(name = "dish_update_by")
    public String getDishUpdateBy() {
        return dishUpdateBy;
    }

    public void setDishUpdateBy(String dishUpdateBy) {
        this.dishUpdateBy = dishUpdateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DishEntity that = (DishEntity) o;
        return dishId == that.dishId &&
                dishTypeId == that.dishTypeId &&
                Objects.equals(dishName, that.dishName) &&
                Objects.equals(dishDescription, that.dishDescription) &&
                Objects.equals(imageId, that.imageId) &&
                Objects.equals(dishCreateDate, that.dishCreateDate) &&
                Objects.equals(dishCreateBy, that.dishCreateBy) &&
                Objects.equals(dishUpdateDate, that.dishUpdateDate) &&
                Objects.equals(dishUpdateBy, that.dishUpdateBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishId, dishName, dishTypeId, dishDescription, imageId, dishCreateDate, dishCreateBy, dishUpdateDate, dishUpdateBy);
    }
}
