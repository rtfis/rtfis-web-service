package com.rtfis.webservice.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ms_search_type", schema = "rtfis", catalog = "")
public class MsSearchTypeEntity {
    private int searchTypeId;
    private String searchTypeName;

    @Id
    @Column(name = "search_type_id")
    public int getSearchTypeId() {
        return searchTypeId;
    }

    public void setSearchTypeId(int searchTypeId) {
        this.searchTypeId = searchTypeId;
    }

    @Basic
    @Column(name = "search_type_name")
    public String getSearchTypeName() {
        return searchTypeName;
    }

    public void setSearchTypeName(String searchTypeName) {
        this.searchTypeName = searchTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MsSearchTypeEntity that = (MsSearchTypeEntity) o;
        return searchTypeId == that.searchTypeId &&
                Objects.equals(searchTypeName, that.searchTypeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(searchTypeId, searchTypeName);
    }
}
