package com.rtfis.webservice.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "file", schema = "rtfis", catalog = "")
public class FileEntity {
    private int fileId;
    private String fileGroup;
    private String fileName;
    private int fileSize;
    private byte[] fileData;
    private String fileType;
    private Timestamp fileCreateDate;
    private String fileCreateBy;
    private Timestamp fileUpdateDate;
    private String fileUpdateBy;
    private Collection<UserEntity> usersByFileId;

    public FileEntity() {
    }

    public FileEntity(String fileName, byte[] fileData, String fileType) {
        this.fileName = fileName;
        this.fileData = fileData;
        this.fileType = fileType;
    }

    @Id
    @Column(name = "file_id")
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Basic
    @Column(name = "file_group")
    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    @Basic
    @Column(name = "file_name")
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Basic
    @Column(name = "file_size")
    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    @Lob
    @Basic
    @Column(name = "file_data")
    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    @Basic
    @Column(name = "file_type")
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    @Basic
    @Column(name = "file_create_date")
    public Timestamp getFileCreateDate() {
        return fileCreateDate;
    }

    public void setFileCreateDate(Timestamp fileCreateDate) {
        this.fileCreateDate = fileCreateDate;
    }

    @Basic
    @Column(name = "file_create_by")
    public String getFileCreateBy() {
        return fileCreateBy;
    }

    public void setFileCreateBy(String fileCreateBy) {
        this.fileCreateBy = fileCreateBy;
    }

    @Basic
    @Column(name = "file_update_date")
    public Timestamp getFileUpdateDate() {
        return fileUpdateDate;
    }

    public void setFileUpdateDate(Timestamp fileUpdateDate) {
        this.fileUpdateDate = fileUpdateDate;
    }

    @Basic
    @Column(name = "file_update_by")
    public String getFileUpdateBy() {
        return fileUpdateBy;
    }

    public void setFileUpdateBy(String fileUpdateBy) {
        this.fileUpdateBy = fileUpdateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileEntity that = (FileEntity) o;
        return fileId == that.fileId &&
                fileSize == that.fileSize &&
                Objects.equals(fileGroup, that.fileGroup) &&
                Objects.equals(fileName, that.fileName) &&
                Arrays.equals(fileData, that.fileData) &&
                Objects.equals(fileType, that.fileType) &&
                Objects.equals(fileCreateDate, that.fileCreateDate) &&
                Objects.equals(fileCreateBy, that.fileCreateBy) &&
                Objects.equals(fileUpdateDate, that.fileUpdateDate) &&
                Objects.equals(fileUpdateBy, that.fileUpdateBy);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(fileId, fileGroup, fileName, fileSize, fileType, fileCreateDate, fileCreateBy, fileUpdateDate, fileUpdateBy);
        result = 31 * result + Arrays.hashCode(fileData);
        return result;
    }

    @OneToMany(mappedBy = "fileByFileId")
    public Collection<UserEntity> getUsersByFileId() {
        return usersByFileId;
    }

    public void setUsersByFileId(Collection<UserEntity> usersByFileId) {
        this.usersByFileId = usersByFileId;
    }
}
