package com.rtfis.webservice.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ms_dish_type", schema = "rtfis", catalog = "")
public class MsDishTypeEntity {
    private int dishTypeId;
    private String dishTypeName;

    @Id
    @Column(name = "dish_type_id")
    public int getDishTypeId() {
        return dishTypeId;
    }

    public void setDishTypeId(int dishTypeId) {
        this.dishTypeId = dishTypeId;
    }

    @Basic
    @Column(name = "dish_type_name")
    public String getDishTypeName() {
        return dishTypeName;
    }

    public void setDishTypeName(String dishTypeName) {
        this.dishTypeName = dishTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MsDishTypeEntity that = (MsDishTypeEntity) o;
        return dishTypeId == that.dishTypeId &&
                Objects.equals(dishTypeName, that.dishTypeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishTypeId, dishTypeName);
    }
}
