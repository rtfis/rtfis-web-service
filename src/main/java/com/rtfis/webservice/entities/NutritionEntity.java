package com.rtfis.webservice.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "nutrition", schema = "rtfis", catalog = "")
public class NutritionEntity {
    private String nutritionId;
    private String nutritionType;

    @Id
    @Column(name = "nutrition_id")
    public String getNutritionId() {
        return nutritionId;
    }

    public void setNutritionId(String nutritionId) {
        this.nutritionId = nutritionId;
    }

    @Basic
    @Column(name = "nutrition_type")
    public String getNutritionType() {
        return nutritionType;
    }

    public void setNutritionType(String nutritionType) {
        this.nutritionType = nutritionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NutritionEntity that = (NutritionEntity) o;
        return Objects.equals(nutritionId, that.nutritionId) &&
                Objects.equals(nutritionType, that.nutritionType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nutritionId, nutritionType);
    }
}
