package com.rtfis.webservice.dao;

import com.rtfis.webservice.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<UserEntity,Long> {
    @Query(value = "select * from user where user_email =:userEmail and user_password =:userPassword",nativeQuery = true)
    UserEntity getUserByEmailAddPassword(@Param("userEmail") String userEmail,@Param("userPassword") String userPassword);
}
