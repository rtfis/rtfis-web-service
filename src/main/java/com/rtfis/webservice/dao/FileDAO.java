package com.rtfis.webservice.dao;

import com.rtfis.webservice.entities.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileDAO extends JpaRepository<FileEntity,Integer> {

}
