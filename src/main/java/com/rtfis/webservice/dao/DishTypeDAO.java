package com.rtfis.webservice.dao;

import com.rtfis.webservice.entities.MsDishTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DishTypeDAO extends JpaRepository<MsDishTypeEntity,Long> {
}
