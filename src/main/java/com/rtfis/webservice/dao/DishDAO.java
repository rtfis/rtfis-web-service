package com.rtfis.webservice.dao;

import com.rtfis.webservice.entities.DishEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DishDAO extends JpaRepository<DishEntity,Long> {

}
