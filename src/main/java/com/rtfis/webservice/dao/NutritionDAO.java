package com.rtfis.webservice.dao;

import com.rtfis.webservice.entities.NutritionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NutritionDAO extends JpaRepository<NutritionEntity,String> {
}
