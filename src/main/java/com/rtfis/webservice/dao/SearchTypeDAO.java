package com.rtfis.webservice.dao;

import com.rtfis.webservice.entities.MsSearchTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchTypeDAO extends JpaRepository<MsSearchTypeEntity,Long> {
}
