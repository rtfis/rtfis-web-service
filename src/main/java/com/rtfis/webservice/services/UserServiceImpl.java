package com.rtfis.webservice.services;

import com.rtfis.webservice.bean.UserDTO;
import com.rtfis.webservice.dao.UserDAO;
import com.rtfis.webservice.entities.FileEntity;
import com.rtfis.webservice.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static String UPLOADED_FOLDER = "D://Film//";

    @Autowired
    FileService fileService;

    @Autowired
    UserDAO userDAO;

    @Override
    public List<UserEntity> listAll() {
        return userDAO.findAll();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void register(UserDTO user) {
        try {
            FileEntity file = fileService.storeFile(user.getUserPic(), "system", "user");
            UserEntity userEntity = new UserEntity();
            userEntity.setFileId(file.getFileId());
            userEntity.setUserEmail(user.getUserEmail());
            userEntity.setUserName(user.getUserName());
            userEntity.setUserLastName(user.getUserLastName());
            userEntity.setUserPassword(user.getUserPassword());
            userEntity.setUserActivatedFlag("N");
            userEntity.setUserCreateBy("system");
            userEntity.setUserCreateDate(new Timestamp(System.currentTimeMillis()));
            userEntity.setUserUpdateBy("system");

            Path path = Paths.get(UPLOADED_FOLDER + file.getFileName());
            Files.write(path, file.getFileData());

            userDAO.save(userEntity);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

    }

    @Override
    public UserEntity login(String userEmail, String userPassword) {
        return userDAO.getUserByEmailAddPassword(userEmail, userPassword);
    }
}
