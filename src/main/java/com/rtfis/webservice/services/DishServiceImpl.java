package com.rtfis.webservice.services;

import com.rtfis.webservice.bean.DishDTO;
import com.rtfis.webservice.dao.DishDAO;
import com.rtfis.webservice.entities.DishEntity;
import com.rtfis.webservice.entities.FileEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class DishServiceImpl implements DishService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DishServiceImpl.class);

    @Autowired
    FileService fileService;

    @Autowired
    DishDAO dishDAO;

    @Override
    public void add(DishDTO dish) {
        FileEntity file = fileService.storeFile(dish.getDishPic(), "system", "dish");
        DishEntity dishEntity = new DishEntity();
        dishEntity.setDishName(dish.getDishName());
        dishEntity.setDishDescription(dish.getDishDescription());
        dishEntity.setImageId(file.getFileId());
        dishEntity.setDishCreateBy(dish.getUserName());
        dishEntity.setDishCreateDate(new Date(System.currentTimeMillis()));
        dishEntity.setDishUpdateBy(dish.getUserName());
        dishDAO.save(dishEntity);
    }
}
