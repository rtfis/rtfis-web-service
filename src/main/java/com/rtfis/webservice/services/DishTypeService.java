package com.rtfis.webservice.services;

import com.rtfis.webservice.entities.MsDishTypeEntity;

import java.util.List;

public interface DishTypeService {
    List<MsDishTypeEntity> getAllDishType();
}
