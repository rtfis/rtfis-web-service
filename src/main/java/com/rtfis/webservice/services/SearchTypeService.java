package com.rtfis.webservice.services;

import com.rtfis.webservice.entities.MsSearchTypeEntity;

import java.util.List;

public interface SearchTypeService {
    List<MsSearchTypeEntity> getAllSearchType();
}
