package com.rtfis.webservice.services;

import com.rtfis.webservice.bean.UserDTO;
import com.rtfis.webservice.entities.UserEntity;

import java.util.List;

public interface UserService {
    List<UserEntity> listAll();
    void register(UserDTO user);
    UserEntity login(String userEmail,String userPassword);
}
