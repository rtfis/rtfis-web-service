package com.rtfis.webservice.services;

import com.rtfis.webservice.dao.FileDAO;
import com.rtfis.webservice.entities.FileEntity;
import com.rtfis.webservice.exception.FileStorageException;
import com.rtfis.webservice.exception.MyFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;

@Service
public class FileServiceImpl implements FileService {
    @Autowired
    FileDAO fileDAO;

    @Override
    public FileEntity storeFile(MultipartFile file, String userName, String fileGroup) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            FileEntity imageFile = new FileEntity(fileName, file.getBytes(),file.getContentType());
            imageFile.setFileSize(Math.toIntExact(file.getSize()));
            imageFile.setFileCreateBy(userName);
            imageFile.setFileCreateDate(new Timestamp(System.currentTimeMillis()));
            imageFile.setFileGroup(fileGroup);
            imageFile.setFileUpdateBy(userName);
            return fileDAO.save(imageFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public FileEntity getFile(int fileId) {
        return fileDAO.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }
}
