package com.rtfis.webservice.services;

import com.rtfis.webservice.entities.NutritionEntity;

import java.util.List;

public interface NutritionService {
    List<NutritionEntity> getAllNutrition();
}
