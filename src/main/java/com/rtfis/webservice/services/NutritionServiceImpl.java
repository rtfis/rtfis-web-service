package com.rtfis.webservice.services;

import com.rtfis.webservice.dao.NutritionDAO;
import com.rtfis.webservice.entities.NutritionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class NutritionServiceImpl implements NutritionService {

    @Autowired
    private NutritionDAO nutritionDAO;

    @Override
    @Transactional
    public List<NutritionEntity> getAllNutrition() {
        return nutritionDAO.findAll();
    }
}
