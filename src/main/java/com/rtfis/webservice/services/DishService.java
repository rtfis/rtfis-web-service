package com.rtfis.webservice.services;

import com.rtfis.webservice.bean.DishDTO;

public interface DishService {
    void add(DishDTO dish);
}
