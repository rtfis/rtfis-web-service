package com.rtfis.webservice.services;

import com.rtfis.webservice.entities.FileEntity;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    FileEntity storeFile(MultipartFile file , String userName, String fileGroup);
    FileEntity getFile(int fileId);
}
