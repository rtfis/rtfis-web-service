package com.rtfis.webservice.services;

import com.rtfis.webservice.dao.SearchTypeDAO;
import com.rtfis.webservice.entities.MsSearchTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchTypeServiceImpl implements SearchTypeService{

    @Autowired
    private SearchTypeDAO searchTypeDAO;

    @Override
    public List<MsSearchTypeEntity> getAllSearchType() {
        return searchTypeDAO.findAll();
    }
}
