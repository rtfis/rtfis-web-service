package com.rtfis.webservice.services;

import com.rtfis.webservice.dao.DishTypeDAO;
import com.rtfis.webservice.entities.MsDishTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishTypeServiceImpl implements DishTypeService {

    @Autowired
    DishTypeDAO dishTypeDAO;


    @Override
    public List<MsDishTypeEntity> getAllDishType() {
        return dishTypeDAO.findAll();
    }
}
