package com.rtfis.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RtfisWebServiceApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(RtfisWebServiceApplication.class, args);
    }

}
