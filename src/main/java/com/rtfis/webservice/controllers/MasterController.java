package com.rtfis.webservice.controllers;

import com.rtfis.webservice.entities.MsDishTypeEntity;
import com.rtfis.webservice.entities.MsSearchTypeEntity;
import com.rtfis.webservice.services.DishTypeService;
import com.rtfis.webservice.services.SearchTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MasterController {

    @Autowired
    private SearchTypeService searchTypeService;

    @Autowired
    private DishTypeService dishTypeService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/search-type")
    public List<MsSearchTypeEntity> getAllSearchType() {
        return searchTypeService.getAllSearchType();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/dish-type")
    public List<MsDishTypeEntity> getAllDishType() {
        return dishTypeService.getAllDishType();
    }
}
