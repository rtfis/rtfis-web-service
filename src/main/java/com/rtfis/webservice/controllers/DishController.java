package com.rtfis.webservice.controllers;

import com.rtfis.webservice.bean.DishDTO;
import com.rtfis.webservice.services.DishService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class DishController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DishController.class);

    @Autowired
    DishService dishService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "dish/add")
    public ResponseEntity<String> add(@RequestParam("dishPic") MultipartFile dishPic,
                                      @RequestParam("dishFileGroup") String dishFileGroup,
                                      @RequestParam("dishName") String dishName,
                                      @RequestParam("dishDescription") String dishDescription,
                                      @RequestParam("dishTypeId") String userName)throws Exception {
        try {
            DishDTO dish = new DishDTO();
            dish.setDishPic(dishPic);
            dish.setDishFileGroup(dishFileGroup);
            dish.setDishName(dishName);
            dish.setDishDescription(dishDescription);
            dish.setUserName(userName);
            dishService.add(dish);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }

    }
}
